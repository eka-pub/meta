package meta;

import com.google.gson.Gson;
import flight.FlightProvider;

public class MetaApiProvider {
    private static final String URL = "https://provider.dice.tech/";

    public static Gson gson = new Gson();

    public  String origin;
    public  String apiKey;
    public  String token;

    public  MetaApiProvider(String origin,String apiKey,String token){
        this.origin = origin;
        this.apiKey = apiKey;
        this.token = token;
    }

    FlightProvider sandbox;

    public  FlightProvider sandbox(){
        if(sandbox==null){
            sandbox = new FlightProvider(URL + "flight/sandbox/",this);
        }
        return sandbox;
    }


}
