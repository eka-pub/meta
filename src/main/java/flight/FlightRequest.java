package flight;

import flight.request.OneWaySearch.Count;
import flight.request.OneWaySearch.OneWaySearchRequest;

public class FlightRequest {

    private String origin;

    private String destination;

    private Long adult = 1L;

    private String date;

    private Long child = 0L;

    private Long infant = 0L;

    public OneWaySearchRequest oneWaySearchRequest(){
        return new OneWaySearchRequest(new Count(adult,child,infant),date,destination,origin);
    }

    public FlightRequest(String origin, String destination,String date, Long adult, Long child, Long infant) {
        this.origin = origin;
        this.destination = destination;
        this.adult = adult;
        this.date = date;
        this.child = child;
        this.infant = infant;
    }

    public static class Builder{

        private String origin;

        private String date;

        private String destination;

        private Long adult = 1L;

        private Long child = 0L;

        private Long infant = 0L;

        public Builder origin(String origin){
            this.origin = origin;
            return this;
        }

        public Builder date(String date){
            this.date = date;
            return this;
        }

        public Builder destination(String destination){
            this.destination = destination;
            return this;
        }

        public Builder adult(Long adult){
            this.adult = adult;
            return this;
        }

        public Builder child(Long child){
            this.child = child;
            return this;
        }

        public Builder infant(Long infant){
            this.infant = infant;
            return this;
        }

        public FlightRequest build(){
            return new FlightRequest(origin,destination,date,adult,child,infant);
        }

    }


    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Long getAdult() {
        return adult;
    }

    public void setAdult(Long adult) {
        this.adult = adult;
    }

    public Long getChild() {
        return child;
    }

    public void setChild(Long child) {
        this.child = child;
    }

    public Long getInfant() {
        return infant;
    }

    public void setInfant(Long infant) {
        this.infant = infant;
    }
}
