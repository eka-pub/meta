
package flight.request.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OneWaySearchRequest {

    @SerializedName("count")
    private Count mCount;
    @SerializedName("date")
    private String mDate;
    @SerializedName("destination")
    private String mDestination;
    @SerializedName("origin")
    private String mOrigin;

    public OneWaySearchRequest() {
    }

    public OneWaySearchRequest(Count mCount, String mDate, String mDestination, String mOrigin) {
        this.mCount = mCount;
        this.mDate = mDate;
        this.mDestination = mDestination;
        this.mOrigin = mOrigin;
    }

    public Count getCount() {
        return mCount;
    }

    public void setCount(Count count) {
        mCount = count;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

}
