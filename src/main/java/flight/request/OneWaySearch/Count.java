
package flight.request.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Count {

    @SerializedName("adult")
    private Long mAdult;
    @SerializedName("child")
    private Long mChild;
    @SerializedName("infant")
    private Long mInfant;

    public Count(Long mAdult, Long mChild, Long mInfant) {
        this.mAdult = mAdult;
        this.mChild = mChild;
        this.mInfant = mInfant;
    }

    public Long getAdult() {
        return mAdult;
    }

    public void setAdult(Long adult) {
        mAdult = adult;
    }

    public Long getChild() {
        return mChild;
    }

    public void setChild(Long child) {
        mChild = child;
    }

    public Long getInfant() {
        return mInfant;
    }

    public void setInfant(Long infant) {
        mInfant = infant;
    }

}
