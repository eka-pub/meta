package flight;

import com.google.gson.Gson;
import flight.response.ApiErrorResponse;
import flight.response.OneWaySearch.OneWaySearchResponse;
import meta.MetaApiProvider;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class FlightProvider {

    public String url;
    public MetaApiProvider provider;

    public FlightProvider(String url, MetaApiProvider provider) {
        this.url = url;
        this.provider = provider;
    }

    private DomesticFlight domesticFlight;

    public DomesticFlight getDomesticFlight() {
        if(domesticFlight==null){
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(chain -> {
                        Request request = chain.request().newBuilder()
                                .addHeader("X-Api-Key", provider.apiKey)
                                .addHeader("Origin", provider.origin)
                                .addHeader("Authorization", "Bearer "+provider.token)
                                .build();
                        return chain.proceed(request);
                    })
                    .readTimeout(180, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl("https://provider.dice.tech/")
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
            domesticFlight = retrofit.create(DomesticFlight.class);
        }
        return domesticFlight;
    }

    public OneWaySearchResponse search(FlightRequest request){
        Call<OneWaySearchResponse> call = getDomesticFlight().search(request.oneWaySearchRequest());
        try {
            Response<OneWaySearchResponse> response = call.execute();
            if(response.isSuccessful()){
                return response.body();
            }else{
                ApiErrorResponse errorResponse = MetaApiProvider.gson.fromJson(response.errorBody().string(),ApiErrorResponse.class);
                OneWaySearchResponse oneWaySearchResponse = new OneWaySearchResponse();
                oneWaySearchResponse.setSuccess(false);
                oneWaySearchResponse.setMessage(errorResponse.getMessage());
                return oneWaySearchResponse;
            }
        } catch (IOException e) {
            OneWaySearchResponse response = new OneWaySearchResponse();
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

}
