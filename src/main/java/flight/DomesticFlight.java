package flight;

import flight.request.OneWaySearch.OneWaySearchRequest;
import flight.response.OneWaySearch.OneWaySearchResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface DomesticFlight {

    @POST("domestic/search")
    Call<OneWaySearchResponse> search(@Body OneWaySearchRequest request);

}
