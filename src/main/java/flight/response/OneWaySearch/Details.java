
package flight.response.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Details {

    @Expose
    private Long arrival;
    @Expose
    private Long departure;
    @Expose
    private Long duration;
    @Expose
    private Long numberOfStops;

    public Long getArrival() {
        return arrival;
    }

    public void setArrival(Long arrival) {
        this.arrival = arrival;
    }

    public Long getDeparture() {
        return departure;
    }

    public void setDeparture(Long departure) {
        this.departure = departure;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getNumberOfStops() {
        return numberOfStops;
    }

    public void setNumberOfStops(Long numberOfStops) {
        this.numberOfStops = numberOfStops;
    }

}
