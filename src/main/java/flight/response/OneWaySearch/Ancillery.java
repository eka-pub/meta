
package flight.response.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Ancillery {

    @Expose
    private Boolean dateChange;
    @Expose
    private Boolean meal;
    @Expose
    private Boolean seat;

    public Boolean getDateChange() {
        return dateChange;
    }

    public void setDateChange(Boolean dateChange) {
        this.dateChange = dateChange;
    }

    public Boolean getMeal() {
        return meal;
    }

    public void setMeal(Boolean meal) {
        this.meal = meal;
    }

    public Boolean getSeat() {
        return seat;
    }

    public void setSeat(Boolean seat) {
        this.seat = seat;
    }

}
