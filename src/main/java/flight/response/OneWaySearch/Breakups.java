
package flight.response.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Breakups {

    @SerializedName("Carrier Misc")
    private Double carrierMisc;
    @SerializedName("Fuel Surcharge")
    private Double fuelSurcharge;
    @SerializedName("Gst Charges")
    private Double gstCharges;
    @SerializedName("Management Fee Tax")
    private Double managementFeeTax;
    @SerializedName("Management Fees")
    private Double managementFees;
    @SerializedName("Other Charges")
    private Double otherCharges;

    public Double getCarrierMisc() {
        return carrierMisc;
    }

    public void setCarrierMisc(Double carrierMisc) {
        this.carrierMisc = carrierMisc;
    }

    public Double getFuelSurcharge() {
        return fuelSurcharge;
    }

    public void setFuelSurcharge(Double fuelSurcharge) {
        this.fuelSurcharge = fuelSurcharge;
    }

    public Double getGstCharges() {
        return gstCharges;
    }

    public void setGstCharges(Double gstCharges) {
        this.gstCharges = gstCharges;
    }

    public Double getManagementFeeTax() {
        return managementFeeTax;
    }

    public void setManagementFeeTax(Double managementFeeTax) {
        this.managementFeeTax = managementFeeTax;
    }

    public Double getManagementFees() {
        return managementFees;
    }

    public void setManagementFees(Double managementFees) {
        this.managementFees = managementFees;
    }

    public Double getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(Double otherCharges) {
        this.otherCharges = otherCharges;
    }

}
