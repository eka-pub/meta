
package flight.response.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Adult {

    @Expose
    private Ancillery ancillery;
    @Expose
    private Baggage baggage;
    @Expose
    private Double baseFare;
    @Expose
    private String bookingClass;
    @Expose
    private CancellationCharges cancellationCharges;
    @Expose
    private Double comission;
    @Expose
    private Double offeredFare;
    @Expose
    private Double publishedFare;
    @Expose
    private Long remainingSeats;
    @Expose
    private Taxes taxes;

    public Ancillery getAncillery() {
        return ancillery;
    }

    public void setAncillery(Ancillery ancillery) {
        this.ancillery = ancillery;
    }

    public Baggage getBaggage() {
        return baggage;
    }

    public void setBaggage(Baggage baggage) {
        this.baggage = baggage;
    }

    public Double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(Double baseFare) {
        this.baseFare = baseFare;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public CancellationCharges getCancellationCharges() {
        return cancellationCharges;
    }

    public void setCancellationCharges(CancellationCharges cancellationCharges) {
        this.cancellationCharges = cancellationCharges;
    }

    public Double getComission() {
        return comission;
    }

    public void setComission(Double comission) {
        this.comission = comission;
    }

    public Double getOfferedFare() {
        return offeredFare;
    }

    public void setOfferedFare(Double offeredFare) {
        this.offeredFare = offeredFare;
    }

    public Double getPublishedFare() {
        return publishedFare;
    }

    public void setPublishedFare(Double publishedFare) {
        this.publishedFare = publishedFare;
    }

    public Long getRemainingSeats() {
        return remainingSeats;
    }

    public void setRemainingSeats(Long remainingSeats) {
        this.remainingSeats = remainingSeats;
    }

    public Taxes getTaxes() {
        return taxes;
    }

    public void setTaxes(Taxes taxes) {
        this.taxes = taxes;
    }

}
