
package flight.response.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

import java.util.HashMap;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Taxes {

    @Expose
    private HashMap<String,Double> breakups;
    @Expose
    private Double total;

    public HashMap<String,Double> getBreakups() {
        return breakups;
    }

    public void setBreakups(HashMap<String,Double> breakups) {
        this.breakups = breakups;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
