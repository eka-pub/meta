
package flight.response.OneWaySearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Fare {

    @Expose
    private Adult adult;
    @Expose
    private Adult child;
    @Expose
    private Adult infant;
    @Expose
    private String appId;
    @Expose
    private String key;
    @Expose
    private String message;
    @Expose
    private Boolean override;
    @Expose
    private String type;
    @Expose
    private String typeKey;

    public Adult getAdult() {
        return adult;
    }

    public void setAdult(Adult adult) {
        this.adult = adult;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getOverride() {
        return override;
    }

    public void setOverride(Boolean override) {
        this.override = override;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeKey() {
        return typeKey;
    }

    public void setTypeKey(String typeKey) {
        this.typeKey = typeKey;
    }

    public Adult getChild() {
        return child;
    }

    public void setChild(Adult child) {
        this.child = child;
    }

    public Adult getInfant() {
        return infant;
    }

    public void setInfant(Adult infant) {
        this.infant = infant;
    }
}
